from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
import requests
import environ
from .omdb_api import get_detail_movie, create_json_from_dict, search_movie, get_api_key
from lab_9.csui_helper import get_access_token, verify_user, get_client_id , get_data_user

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

class Lab10Test(TestCase):
	def test_lab_10_url_is_exist(self):
		response = Client().get('/lab-10/')
		self.assertEqual(response.status_code,200)

	def test_lab_10_using_index_func(self):
		found = resolve('/lab-10/')
		self.assertEqual(found.func, index)

	def test_lab_10_login_session(self):
		session = self.client.session
		session['user_login'] = 'fairuz.yassar'
		session.save()
		response = self.client.get('/lab-10/')
		self.assertEqual(response.status_code,302)

	def test_lab_10_not_login(self):
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 302)


	def test_lab_10_dashboard_login(self):
		session = self.client.session
		session['user_login'] = 'fairuz.yassar'
		session['kode_identitas'] = '1606878505'
		session['role'] = 'mahasiswa'
		session['movies'] = 'storage'
		session.save()
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 200)

	def test_lab_10_dashboard_login_without_attribute(self):
		response = self.client.get('/lab-10/movie/list/')
		self.assertEqual(response.status_code, 302)
		session = self.client.session
		session['user_login'] = 'fairuz.yassar'
		session['kode_identitas'] = '1606917941'
		session['role'] = 'mahasiswa'
		session.save()
		response = self.client.get('/lab-10/movie/list/')
		response = self.client.get('/lab-10/movie/list/?judul=Kingsman&tahun=2017')
		self.assertEqual(response.status_code, 200)

	def test_lab_10_movie_details_not_login(self):
		response = self.client.get('/lab-10/movie/detail/tt1396484/')
		self.assertEqual(response.status_code, 200)

	def test_lab_10_movie_details_login(self):
		session = self.client.session
		session['user_login'] = 'fairuz.yassar'
		session['kode_identitas'] = '1606917941'
		session['role'] = 'mahasiswa'
		session.save()
		response = self.client.get('/lab-10/dashboard/')
		response = self.client.get('/lab-10/movie/detail/tt1396484/')
		self.assertEqual(response.status_code, 200)

	def test_lab_10_add_watch_later_not_login(self):
		response = self.client.get('/lab-10/movie/watch_later/add/tt1396484/')
		response = self.client.get('/lab-10/movie/watch_later/add/tt1396484/')
		self.assertEqual(response.status_code, 302)


	def test_lab_10_add_watch_later_movie_session(self):
		session = self.client.session
		session['movies'] = []
		session.save()
		response = self.client.get('/lab-10/movie/watch_later/add/tt1396484/')
		response = self.client.get('/lab-10/movie/watch_later/add/tt1396484/')
		self.assertEqual(response.status_code, 302)

	def test_lab_10_add_watch_later_login(self):
		session = self.client.session
		session['user_login'] = 'fairuz.yassar'
		session['kode_identitas'] = '1606917941'
		session['role'] = 'mahasiswa'
		session.save()
		response = self.client.get('/lab-10/dashboard/')
		response = self.client.get('/lab-10/movie/watch_later/add/tt1396484/')
		response = self.client.get('/lab-10/movie/watch_later/add/tt1396484/')
		self.assertEqual(response.status_code, 302)

	def test_lab_10_list_watch_later_not_login(self):
		response = self.client.get('/lab-10/movie/watch_later/')
		self.assertEqual(response.status_code, 302)

	def test_lab_10_list_watch_later_login(self):
		session = self.client.session
		session['user_login'] = 'fairuz.yassar'
		session['kode_identitas'] = '1606917941'
		session['role'] = 'mahasiswa'
		session.save()
		response = self.client.get('/lab-10/dashboard/')
		response = self.client.get('/lab-10/movie/watch_later/')
		self.assertEqual(response.status_code, 200)

	def test_lab_10_list_watch_later_movies_double(self):
		session = self.client.session
		session['user_login'] = 'fairuz.yassar'
		session['kode_identitas'] = '1606917941'
		session['role'] = 'mahasiswa'
		session.save()
		response = self.client.get('/lab-10/dashboard/')
		response = self.client.get('/lab-10/movie/watch_later/add/tt1396484/')
		response = self.client.get('/lab-10/movie/watch_later/add/tt1396484/')
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/movie/watch_later/')

	def test_lab_10_search_movie_strip(self):
		response = self.client.get('/lab-10/api/movie/-/-/')
		self.assertEqual(response.status_code, 200)
		
	def test_lab_10_search_movie_exsist(self):
		response = self.client.get('/lab-10/api/movie/batman/-/')
		response = self.client.get('/lab-10/api/movie/kingsman/2017/')
		self.assertEqual(response.status_code, 200)

	def test_get_api_key(self):
		self.assertEqual(get_api_key(),"f2c5bbae")

	def test_get_detail_movie(self):
		response = requests.get("http://www.omdbapi.com/?i=tt1396484&apikey="+get_api_key())
		response = response.json()
		response = create_json_from_dict(response)
		self.assertEqual(response,get_detail_movie("tt1396484"))

	def test_search_non_exist_movie(self):
		response = search_movie("asdf","-")
		self.assertEqual(len(response),0)

	def test_search_exist_movie(self):
		response = search_movie("Kingsman: The Golden Circle","2017")
		self.assertTrue(len(response)>0)

	def test_lab_10_auth_login(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		response = Client().post('/lab-10/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)

	def test_lab_10_auth_login_wrong(self):
		response = Client().post('/lab-10/custom_auth/login/', {'username': 'test', 'password': 'test'})
		response = self.client.get('/lab-10/')

	def test_lab_10_auth_logout(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		response_login =  self.client.post('/lab-10/custom_auth/login/', {'username': self.username, 'password': self.password})
		response = self.client.get('/lab-10/custom_auth/logout/')



		


