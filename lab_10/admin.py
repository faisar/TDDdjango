from django.contrib import admin

# Register your models here.
from .models import Pengguna, MovieKu
admin.site.register(Pengguna)
admin.site.register(MovieKu)