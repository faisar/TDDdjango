from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

landing_page_content = 'welcome to my first website i create today lets enjoy it'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)
