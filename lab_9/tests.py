from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from http.cookies import SimpleCookie
from .csui_helper import get_access_token, verify_user, get_client_id , get_data_user
import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

class Lab9Test(TestCase):
	def test_lab_9_url_is_exist(self):
		response = Client().get('/lab-9/')
		self.assertEqual(response.status_code,200)

	def test_lab_9_using_index_func(self):
		found = resolve('/lab-9/')
		self.assertEqual(found.func, index)

	def test_login_session_lab_9(self):
		session = self.client.session
		session['user_login'] = 'fairuz.yassar'
		session.save()
		response = self.client.get('/lab-9/')
		self.assertEqual(response.status_code,302)

	def test_lab_9_not_login(self):
		response = self.client.get('/lab-9/profile/')
		self.assertEqual(response.status_code, 302)

	def test_lab_9_profile_login_without_attribute(self):
		session = self.client.session
		session['user_login'] = 'fairuz.yassar'
		session['access_token'] = 'access'
		session['kode_identitas'] = '1606917941'
		session['role'] = 'mahasiswa'
		session.save()
		response = self.client.get('/lab-9/profile/')
		self.assertEqual(response.status_code, 200)

	def test_lab_9_profile_login(self):
		session = self.client.session
		session['user_login'] = 'fairuz.yassar'
		session['access_token'] = 'access'
		session['kode_identitas'] = '1606917941'
		session['role'] = 'mahasiswa'
		session['drones'] = []
		session['soundcards'] =[]
		session['opticals'] = []
		session.save()
		response = self.client.get('/lab-9/profile/')
		self.assertEqual(response.status_code, 200)

	def test_lab_9_add_drones(self):
		response = self.client.get('/lab-9/add_session_drones/1/')
		self.assertEqual(response.status_code, 302)

	def test_lab_9_add_soundcards(self):
		response = self.client.get('/lab-9/add_session_soundcards/1/')
		self.assertEqual(response.status_code, 302)

	def test_lab_9_add_opticals(self):
		response = self.client.get('/lab-9/add_session_opticals/1/')
		self.assertEqual(response.status_code, 302)

	def test_lab_9_add_drones_2(self):
		session = self.client.session
		session['drones'] = ['1', '2', '3']
		session.save()
		response = self.client.get('/lab-9/add_session_drones/4/')

	def test_lab_9_add_soundcard_2(self):
		session = self.client.session
		session['soundcards'] = ['1', '2', '3']
		session.save()
		response = self.client.get('/lab-9/add_session_soundcards/4/')

	def test_lab_9_add_opticals_2(self):
		session = self.client.session
		session['opticals'] = ['1', '2', '3']
		session.save()
		response = self.client.get('/lab-9/add_session_opticals/4/')

	def test_lab_9_del_drones(self):
		session = self.client.session
		session['drones'] = ['1', '2', '3']
		session.save()
		response = self.client.get('/lab-9/del_session_drones/1/')

	def test_lab_9_del_soundcards(self):
		session = self.client.session
		session['soundcards'] = ['1', '2', '3']
		session.save()
		response = self.client.get('/lab-9/del_session_soundcards/1/')

	def test_lab_9_del_opticals(self):
		session = self.client.session
		session['opticals'] = ['1', '2', '3']
		session.save()
		response = self.client.get('/lab-9/del_session_opticals/1/')

	def test_lab_9_clear_drones(self):
		session = self.client.session
		session['drones'] = ['1', '2', '3']
		session.save()
		response = self.client.get('/lab-9/clear_session_drones/')

	def test_lab_9_clear_soundcards(self):
		session = self.client.session
		session['soundcards'] = ['1', '2', '3']
		session.save()
		response = self.client.get('/lab-9/clear_session_soundcards/')

	def test_lab_9_clear_opticals(self):
		session = self.client.session
		session['opticals'] = ['1', '2', '3']
		session.save()
		response = self.client.get('/lab-9/clear_session_opticals/')

	def test_lab_9_cookies_login_page(self):
		response = self.client.get('/lab-9/cookie/login/')
		self.assertEqual(response.status_code, 200)

	def test_lab_9_cookies_login_render(self):
		cookies = self.client.cookies
		self.client.cookies = SimpleCookie({'user_login': 'utest', 'user_password': 'ptest'})
		response = self.client.get('/lab-9/cookie/login/')
		self.assertEqual(response.status_code, 302)

	def test_lab_9_cookies_auth_login(self):
		response = Client().post('/lab-9/cookie/auth_login/', {'username':'utest', 'password':'ptest'})
		self.assertEqual(response.status_code, 302)

	def test_lab_9_cookies_auth_login_wrong(self):
		response = Client().post('/lab-9/cookie/auth_login/', {'username':'test', 'password':'test'})
		self.assertEqual(response.status_code, 302)

	def test_lab_9_cookies_auth_login_else(self):
		response = self.client.get('/lab-9/cookie/auth_login/')
		self.assertEqual(response.status_code, 302)

	def test_lab_9_cookies_profile_not_login(self):
		response = self.client.get('/lab-9/cookie/profile/')
		self.assertEqual(response.status_code, 302)

	def test_lab_9_cookies_profile_login(self):
		self.client.cookies = SimpleCookie({'user_login': 'utest', 'user_password': 'ptest',})
		response = self.client.get('/lab-9/cookie/profile/')

	def test_lab_9_cookies_profile_wrong(self):
		self.client.cookies = SimpleCookie({'user_login': 'test', 'user_password': 'test',})
		response = self.client.get('/lab-9/cookie/profile/')

	def test_lab_9_cookies_clear(self):
		self.client.cookies = SimpleCookie({'user_login': 'utest', 'user_password': 'ptest',})
		response = self.client.get('/lab-9/cookie/clear/')

	def test_lab_9_auth_login(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		response = Client().post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)

	def test_lab_9_auth_login_wrong(self):
		response = Client().post('/lab-9/custom_auth/login/', {'username': 'test', 'password': 'test'})
		response = self.client.get('/lab-9/')

	def test_lab_9_auth_logout(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		response_login =  self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		print(response_login)
		response = self.client.get('/lab-9/custom_auth/logout/')
		print(response)

	def test_get_data_user(self):
		access_token = '123456789'
		id = 'test'
		data_user = get_data_user(access_token, id)
		self.assertIn("detail", data_user)
		self.assertEqual(data_user["detail"], "Authentication credentials were not provided.")