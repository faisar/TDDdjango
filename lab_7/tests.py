from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, friend_list, model_to_dict, add_friend, validate_npm
from .models import Friend

class Lab7Test(TestCase):

	def test_lab_7_url_is_exist(self):
		response = Client().get('/lab-7/')
		self.assertEqual(response.status_code,200)
	
	def test_lab_7_using_index_func(self):
		found = resolve('/lab-7/')
		self.assertEqual(found.func, index)
	
	def test_lab7_add_friend_form(self):
		name_adi = 'Adi'
		npm_adi = '1606917941'
		friend = Friend.objects.create(friend_name=name_adi, npm=npm_adi)
		self.assertEqual(str(friend.npm), npm_adi)
	
	def test_lab_7_friend_list_exist(self):
		response = Client().get('/lab-7/get-friend-list/')
		self.assertEqual(response.status_code, 200)
	
	def test_lab_7_using_friend_list_func(self):
		found = resolve('/lab-7/get-friend-list/')
		self.assertEqual(found.func, friend_list)

	def test_lab_7_validate_npm(self):
		response_val = Client().post('/lab-7/validate-npm/', {'npm':'1606917941'})
		response_val2 = Client().post('/lab-7/add-friend/', {'name': 'Fairuz Yassar', 'npm':'1606917941'})
		response_val3 = Client().post('/lab-7/delete-friend/', {'npm':'1606917941'})
		response_val4 = Client().post('/lab-7/friend-data/')
		
	def test_lab_7_add_friend(self):
		friend = Friend.objects.create(friend_name='Fairuz Yassar', npm='1606917941')
		data = model_to_dict(friend)
		self.assertIn('Fairuz Yassar', data)
		self.assertIn('1606917941', data)