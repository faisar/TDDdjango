// Calculator
var print = document.getElementById('print');
var erase = false;
var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = " ";
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else if (x === 'sin') {
    print.value = Math.round(Math.sin(evil(print.value) * Math.PI / 180) * 10000) / 10000;
  } else if (x === 'log') {
    print.value = Math.round(Math.log(evil(print.value))/ Math.log(10) * 10000) / 10000;
  } else if ( x === 'tan'){
    if (Math.round(Math.tan(evil(print.value) * Math.PI / 180) * 10000) / 10000 > 60) {
      print.value = "Math Error";
    }else{
      print.value = Math.round(Math.tan(evil(print.value) * Math.PI / 180) * 10000) / 10000;  
    }
    
  }
  else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END
//hide
var down = document.getElementById("chat-body");
var arrow = document.getElementById("chat-arrow");
function display(){
  if (down.style.display === "block") {
    down.style.display = "none";
    arrow.style.transform = "rotateZ(180deg)";
  }else{
    down.style.display = "block";
    arrow.style.transform = "rotateZ(0deg)";
  }
}
//Chat
var chat = document.getElementById("msg-insert");
var text = document.getElementById("chat-text");
function submit(ele) {
    if(event.key === 'Enter' && ele.value != "") {
      ele.value = "Pesan: "+ ele.value;
      chat.innerHTML += (ele.value+"<br/>");
      ele.value = "";  
    }
}

$(document).ready(function() {
    $('.my-select').select2();
});

if(typeof(Storage) !== "undefined") {
    var themes = [
      {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
      {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
      {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
      {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
      {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
      {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
      {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
      {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
      {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
      {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
      {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
      ];
      localStorage.color = JSON.stringify(themes);
}
      $(document).ready(function() {
        if (localStorage.defaultTheme) {
          var applyTheme = JSON.parse(localStorage.defaultTheme);
          $("body").css({"background-color": applyTheme.nowTheme.bcgColor, "color" : applyTheme.nowTheme.fontColor});

        }else{
          var defaultTheme = {"nowTheme":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};
          localStorage.defaultTheme = JSON.stringify(defaultTheme);
          $("body").css({"background-color": defaultTheme.nowTheme.bcgColor, "color" : defaultTheme.nowTheme.fontColor});
        }
        $('.my-select').select2({
          'data': JSON.parse(localStorage.color)
        });
        $('.apply-button').on('click', function(){  // sesuaikan class button
            // [TODO] ambil value dari elemen select .my-select
            var indexKe = $('.my-select').val();
            // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
            var choosing = themes[indexKe];
            // [TODO] ambil object theme yang dipilih
            // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
             $("body").css({"background-color": choosing.bcgColor, "color" : choosing.fontColor});
            // [TODO] simpan object theme tadi ke local storage selectedTheme
            localStorage.defaultTheme = JSON.stringify({"nowTheme":{"bcgColor" : choosing.bcgColor, "color" : choosing.fontColor}});
        });
      });


